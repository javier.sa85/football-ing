# Technical Test - ING

## Introduction

This is a technical test created for ING

The focus of this technical test is to provide knowledge related to SpringBoot, Java 8 and Maven

In this case is based on a football database

## Structure of the Project

This is a Spring Boot project with the Maven structure based on src/main/java, src/main/resources, src/test/java and src/test/resources structure

### Java classes 

The packages in the implementation to be considered are:
- es.ing.football.team.config: Configuration classes. It has been added Swagger for testing and easy visibility of the API
- es.ing.football.team.controller: Controller class
- es.ing.football.team.service: Service adapter between controller and the repository
- es.ing.football.team.entity: Entity with the Data expected by the DDBB
- es.ing.football.team.repository: Bean used for interaction with clients. API REST interactions
- es.ing.football.team.repository: Basic repository based on PagingAndSortingRepository
- es.ing.football.team.exceptions: Contains global exception handler for demonstration of an approach with several constraint violations generated 
due to a bad request
- es.ing.football.team.security: Security Configuration (Demonstration)

### Resources

As part of the resources it have been provided DB Migration scripts for flyway demonstration

## Implemented Tests

### Unit Testing

- es.ing.football.team.controller.TeamControllerTest -> Unit Testing based on MockMvn testing the different provided APIs. 
- es.ing.football.team.repository.TeamRepositoryTest -> DataJpaTest for demonstration
- es.ing.football.team.service.TeamServiceTest -> Unit test mocking the repository

### Integration Test

- es.ing.football.team.IntegrationTest -> It is a demonstration of a SpringBootTest. Uses TestRestTemplate to call the application.

## Pipeline

**NOTE**: The Virtual Machine with Jenkins and Docker created and used for testing the overall process can be provided on demand. Please contact javier.sanchezarroyo@cognizant.com and provide a location to add the ova file.

The build process is executed into a Docker Image with Maven
Docker steps require the main node or a node with docker installed

## Dockerfile

Dockerfile based on a image with java 8. Runs the application and exposes it on the 8080 port.

## Access to the application

The application is deployed on port 8080. The swagger view can be accessed using http://localhost:8080/swagger-ui/#

## JSON Example for data creation

{
  "city": "Madrid",
  "creationDate": "1901-01-01",
  "league": "La Liga",
  "name": "Real Madrid",
  "numPlayers": 25,
  "owner": "TitoFloren",
  "stadiumCapacity": 80000
}

## HOW-TOs

### How to get all the teams

You can get it accessing http://localhost:8080/teams

### How to get all the teams sorted by stadium capacity

You can get it accessing http://localhost:8080/teams?sortBy=stadiumCapacity

### How to update a Team

First of all you need to know the ID of the team. This allows you to be able to change the name of the team.

You can get all the information about a team accessing http://localhost:8080/teams/<teamName>

Then, to change the information you can provide new information using HTTP PUT verb on http://localhost:8080/teams/<id>

### How to delete a Team

First of all you need to know the ID of the team. This allows you to be able to change the name of the team.

You can get all the information about a team accessing http://localhost:8080/teams/<teamName>

Then, to delete it you can use HTTP DELETE verb on http://localhost:8080/teams/<id>
