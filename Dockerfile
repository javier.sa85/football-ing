FROM java:8
EXPOSE 8080
ADD /target/team-0.0.1-SNAPSHOT.jar team.jar
ENTRYPOINT ["java","-jar","team.jar"]