package es.ing.football.team.utils;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;

public class TestConstraintViolation implements ConstraintViolation<Object>{

    private final String message;

    public TestConstraintViolation (String message) {
        this.message = message;
    }

        @Override
        public String getMessage() {
            return this.message;
        }
        
        @Override
        public String getMessageTemplate() {
            return null;
        }
        @Override
        public Object getRootBean() {
            return null;
        }
        @Override
        public Class<Object> getRootBeanClass() {
            return null;
        }
        @Override
        public Object getLeafBean() {
            return null;
        }
        @Override
        public Object[] getExecutableParameters() {
            return null;
        }
        @Override
        public Object getExecutableReturnValue() {
            return null;
        }
        @Override
        public Path getPropertyPath() {
            return null;
        }
        @Override
        public Object getInvalidValue() {
            return null;
        }
        @Override
        public ConstraintDescriptor<?> getConstraintDescriptor() {
            return null;
        }
        @Override
        public <U> U unwrap(Class<U> type) {
            return null;
        }
}
