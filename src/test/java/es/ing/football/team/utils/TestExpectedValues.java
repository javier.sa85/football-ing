package es.ing.football.team.utils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.ing.football.team.entity.Team;
import es.ing.football.team.response.TeamResponse;

public final class TestExpectedValues {
    /**
     * Authentication
     */
    public final static String AUTH_HEADER = "Authorization";
    public final static String AUTH_VALUE = "Basic YWRtaW46YWRtaW4=";

    /**
	 * Team Entity values for testing
     * 
	 */
    public final static Team REALMADRID_ENTITY = new Team(1, "Real Madrid", "Madrid", "TitoFloren", 80000, "LaLiga", 25, LocalDate.of(1902, 3, 6));
    public final static Team BARCELONA_ENTITY = new Team(2, "Barcelona", "Barcelona", "Laporta", 90000, "LaLiga", 23, LocalDate.of(1899, 11, 29));
    public final static Team MAN_UNITED_ENTITY = new Team(3, "Manchester United", "Manchester", "Glazer", 70000, "Premier", 24, LocalDate.of(1878, 11, 29));
    public final static Team JUVENTUS_ENTITY = new Team(4, "Junventus", "Turin", "Agnelli", 60000, "Serie A", 26, LocalDate.of(1897, 11, 1));
    public final static Iterable<Team> ALL_TEAMS_ENTITY = Arrays.asList(REALMADRID_ENTITY, BARCELONA_ENTITY, MAN_UNITED_ENTITY, JUVENTUS_ENTITY);

    public final static Team REALMADRID_ENTITY_FIXED = new Team(1, "Real Madrid", "Madrid", "socios", 80000, "LaLiga", 25, LocalDate.of(1902, 3, 6));
    public final static Team ATLETICO_ENTITY = new Team(5, "Atletico", "Madrid", "Gil Marin", 60000, "LaLiga", 27, LocalDate.of(1903,4,26));

    /**
	 * Team Response values for testing
	 */
    public final static TeamResponse REALMADRID_RESPONSE = TeamResponse.of(REALMADRID_ENTITY);
    public final static TeamResponse BARCELONA_RESPONSE = TeamResponse.of(BARCELONA_ENTITY);
    public final static TeamResponse MAN_UNITED_RESPONSE = TeamResponse.of(MAN_UNITED_ENTITY);
    public final static TeamResponse JUVENTUS_RESPONSE = TeamResponse.of(JUVENTUS_ENTITY);
    public final static List<TeamResponse> ALL_TEAMS_RESPONSE = Arrays.asList(REALMADRID_RESPONSE, BARCELONA_RESPONSE, MAN_UNITED_RESPONSE, JUVENTUS_RESPONSE);

    public final static TeamResponse REALMADRID_RESPONSE_FIXED = TeamResponse.of(REALMADRID_ENTITY_FIXED);
    public final static TeamResponse ATLETICO_RESPONSE = TeamResponse.of(ATLETICO_ENTITY);

	/**
	 * Expected exceptions to be validated
	 */
	public final static TestConstraintViolation NAME_VIOLATION = new TestConstraintViolation("invalid name");
	public final static TestConstraintViolation CITY_VIOLATION = new TestConstraintViolation("invalid city");
	public final static Set<TestConstraintViolation> CONSTRAINT_VIOLATIONS = new HashSet<>(Arrays.asList(NAME_VIOLATION, CITY_VIOLATION));
	public final static String EXPECTED_BODY_ERROR_NAME = "invalid name";
	public final static String EXPECTED_BODY_ERROR_CITY = "invalid city";
}
