package es.ing.football.team.repository;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import es.ing.football.team.entity.Team;
import es.ing.football.team.utils.TestExpectedValues;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TeamRepositoryTest {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void getTeam_returnsTeamEntity() throws Exception {
        Team team = teamRepository.findById(TestExpectedValues.REALMADRID_ENTITY.getId()).get();

        assertThat(team.getId()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getId());
        assertThat(team.getName()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getName());
        assertThat(team.getCity()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getCity());
        assertThat(team.getOwner()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getOwner());
        assertThat(team.getStadiumCapacity()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getStadiumCapacity());
        assertThat(team.getLeague()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getLeague());
        assertThat(team.getNumPlayers()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getNumPlayers());
        assertThat(team.getCreationDate()).isEqualTo(TestExpectedValues.REALMADRID_ENTITY.getCreationDate());
    }
    
}
