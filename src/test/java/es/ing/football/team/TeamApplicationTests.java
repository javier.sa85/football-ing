package es.ing.football.team;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.ing.football.team.controller.TeamController;
import es.ing.football.team.repository.TeamRepository;
import es.ing.football.team.service.TeamService;

@SpringBootTest
class TeamApplicationTests {

	@Autowired
	private TeamController teamController;

	@Autowired 
	private TeamService teamService;

	@Autowired
	private TeamRepository teamRepository;


	@Test
	void contextLoads() {
		assertThat(teamController).isNotNull();
		assertThat(teamService).isNotNull();
		assertThat(teamRepository).isNotNull();
	}
}
