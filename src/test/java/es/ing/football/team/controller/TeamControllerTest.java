package es.ing.football.team.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import javax.validation.ConstraintViolationException;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.ing.football.team.security.WebSecurityConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import es.ing.football.team.response.TeamResponse;
import es.ing.football.team.service.TeamService;
import es.ing.football.team.utils.TestExpectedValues;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.hamcrest.CoreMatchers.containsString;

@RunWith(SpringRunner.class)
@ContextConfiguration
@WebMvcTest(TeamController.class)
public class TeamControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private TeamService teamService;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(context)
				.defaultRequest(get("/").with(user("user").roles("ADMIN")))
				.apply(springSecurity())
				.build();
	}



	@Test
	public void getAllTeams() throws Exception {
		when(teamService.findAllTeams(any(Sort.class))).thenReturn(TestExpectedValues.ALL_TEAMS_RESPONSE);
		this.mockMvc.perform(get("/teams").header(TestExpectedValues.AUTH_HEADER, TestExpectedValues.AUTH_VALUE)).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.[?(@.id =='" + TestExpectedValues.REALMADRID_RESPONSE.getId() + "')].name").value(TestExpectedValues.REALMADRID_RESPONSE.getName()))
				.andExpect(jsonPath("$.[?(@.id =='" + TestExpectedValues.JUVENTUS_RESPONSE.getId() + "')].name").value(TestExpectedValues.JUVENTUS_RESPONSE.getName()));
	}

	@Test
	public void find_RealMadrid() throws Exception {
		when(teamService.findTeamByName(TestExpectedValues.REALMADRID_RESPONSE.getName())).thenReturn(TestExpectedValues.REALMADRID_RESPONSE);
		this.mockMvc.perform(get("/teams/" + TestExpectedValues.REALMADRID_RESPONSE.getName())).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value(TestExpectedValues.REALMADRID_RESPONSE.getName()));
	}

	@Test
	public void createTeamAtletico() throws Exception {
		when(teamService.save(any(TeamResponse.class))).thenReturn(TestExpectedValues.ATLETICO_RESPONSE);
		this.mockMvc.perform(post("/teams").with(csrf())
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsString(TestExpectedValues.ATLETICO_RESPONSE)))
						.andDo(print()).andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value(TestExpectedValues.ATLETICO_RESPONSE.getName()));
	}

	@Test
	public void createSeveralValidationErrors() throws Exception {
		TeamResponse invalidTeam = new TeamResponse();

		when(teamService.save(any(TeamResponse.class))).thenThrow(new ConstraintViolationException(TestExpectedValues.CONSTRAINT_VIOLATIONS));
		this.mockMvc.perform(post("/teams").with(csrf())
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsString(invalidTeam)))
						.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString(TestExpectedValues.EXPECTED_BODY_ERROR_NAME)))
				.andExpect(content().string(containsString(TestExpectedValues.EXPECTED_BODY_ERROR_CITY)));
	}

	@Test
	public void updateRealMadrid() throws Exception {
		when(teamService.updateTeam(any(Long.class), any(TeamResponse.class))).thenReturn(TestExpectedValues.REALMADRID_RESPONSE_FIXED);
		this.mockMvc.perform(put("/teams/"+ TestExpectedValues.REALMADRID_RESPONSE_FIXED.getId()).with(csrf())
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsString(TestExpectedValues.REALMADRID_RESPONSE_FIXED)))
					.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.owner").value(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getOwner()));
	}

	@Test
	public void deleteJuventus() throws Exception {
		this.mockMvc.perform(delete("/teams/4").with(csrf())).andDo(print()).andExpect(status().isNoContent());
	}
	
}