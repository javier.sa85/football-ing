package es.ing.football.team.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import es.ing.football.team.entity.Team;
import es.ing.football.team.exceptions.TeamNotFoundException;
import es.ing.football.team.repository.TeamRepository;
import es.ing.football.team.response.TeamResponse;
import es.ing.football.team.utils.TestExpectedValues;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTest {
    
    @Mock
    private TeamRepository teamRepository;

    private TeamService teamService;

    @Before
    public void setUp() throws Exception {
        teamService = new TeamService(teamRepository);
    }

    @Test
	public void getAllTeams() throws Exception {
		when(teamRepository.findAll(any(Sort.class))).thenReturn(TestExpectedValues.ALL_TEAMS_ENTITY);
        List<TeamResponse> allTeams = teamService.findAllTeams(Sort.by("id"));
        assertThat(allTeams.size()).isEqualTo(4);
	}

	@Test
	public void getATeam_ReturnsTeamResponse() throws Exception {
		when(teamRepository.findByName(TestExpectedValues.REALMADRID_RESPONSE.getName())).thenReturn(TestExpectedValues.REALMADRID_ENTITY);
        TeamResponse teamResponse = this.teamService.findTeamByName(TestExpectedValues.REALMADRID_RESPONSE.getName());

        assertThat(teamResponse.getId()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getId());
        assertThat(teamResponse.getName()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getName());
        assertThat(teamResponse.getCity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getCity());
        //avoid owner assertion as it depends on the order of the test execution
        assertThat(teamResponse.getStadiumCapacity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getStadiumCapacity());
        assertThat(teamResponse.getLeague()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getLeague());
        assertThat(teamResponse.getNumPlayers()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getNumPlayers());
        assertThat(teamResponse.getCreationDate()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getCreationDate());
	}

    @Test(expected = TeamNotFoundException.class)
	public void getATeam_ReturnsTeamNotFound() {
        this.teamService.findTeamByName(TestExpectedValues.REALMADRID_RESPONSE.getName());
	}

	@Test
	public void createTeamAtletico() throws Exception {
        when(teamRepository.save(any(Team.class))).thenReturn(TestExpectedValues.ATLETICO_ENTITY);

		TeamResponse teamResponse = teamService.save(TestExpectedValues.ATLETICO_RESPONSE);

		assertThat(teamResponse.getId()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getId());
        assertThat(teamResponse.getName()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getName());
        assertThat(teamResponse.getCity()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getCity());
        assertThat(teamResponse.getOwner()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getOwner());
        assertThat(teamResponse.getStadiumCapacity()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getStadiumCapacity());
        assertThat(teamResponse.getLeague()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getLeague());
        assertThat(teamResponse.getNumPlayers()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getNumPlayers());
        assertThat(teamResponse.getCreationDate()).isEqualTo(TestExpectedValues.ATLETICO_ENTITY.getCreationDate());
	}

	@Test(expected = ConstraintViolationException.class)
	public void createSeveralValidationErrors() throws Exception {
		TeamResponse invalidTeam = new TeamResponse();
        when(teamRepository.save(any(Team.class))).thenThrow(new ConstraintViolationException(TestExpectedValues.CONSTRAINT_VIOLATIONS));

		teamService.save(invalidTeam);
	}

	@Test
	public void updateRealMadrid() throws Exception {
		when(teamRepository.findById(TestExpectedValues.REALMADRID_ENTITY.getId())).thenReturn(Optional.of(TestExpectedValues.REALMADRID_ENTITY));
		when(teamRepository.save(TestExpectedValues.REALMADRID_ENTITY_FIXED)).thenReturn(TestExpectedValues.REALMADRID_ENTITY_FIXED);

        TeamResponse teamResponse = teamService.updateTeam(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getId(), TestExpectedValues.REALMADRID_RESPONSE_FIXED);

        assertThat(teamResponse.getId()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getId());
        assertThat(teamResponse.getName()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getName());
        assertThat(teamResponse.getCity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getCity());
        assertThat(teamResponse.getOwner()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getOwner());
        assertThat(teamResponse.getStadiumCapacity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getStadiumCapacity());
        assertThat(teamResponse.getLeague()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getLeague());
        assertThat(teamResponse.getNumPlayers()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getNumPlayers());
        assertThat(teamResponse.getCreationDate()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE_FIXED.getCreationDate());
	}

	@Test
	public void deleteJuventus() throws Exception {
        teamService.deleteTeamById(TestExpectedValues.JUVENTUS_ENTITY.getId());
	}

}
