package es.ing.football.team;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import es.ing.football.team.domain.Team;
import es.ing.football.team.repository.TeamRepository;
import es.ing.football.team.utils.TestExpectedValues;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TeamRepository teamRepository;

    @Before
    public void setUp() throws Exception {
        this.teamRepository.save(TestExpectedValues.REALMADRID_ENTITY);
        this.teamRepository.save(TestExpectedValues.BARCELONA_ENTITY);
        this.teamRepository.save(TestExpectedValues.MAN_UNITED_ENTITY);
        this.teamRepository.save(TestExpectedValues.JUVENTUS_ENTITY);
    }


    @Test
    public void getFootballTeam() throws Exception {

        ResponseEntity<Team> response = restTemplate.withBasicAuth("user", "password").getForEntity("/teams/" + TestExpectedValues.REALMADRID_RESPONSE.getName(), Team.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getName()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getName());
        assertThat(response.getBody().getCity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getCity());
        assertThat(response.getBody().getOwner()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getOwner());
        assertThat(response.getBody().getStadiumCapacity()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getStadiumCapacity());
        assertThat(response.getBody().getLeague()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getLeague());
        assertThat(response.getBody().getNumPlayers()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getNumPlayers());
        assertThat(response.getBody().getCreationDate()).isEqualTo(TestExpectedValues.REALMADRID_RESPONSE.getCreationDate());
    }
    
}
