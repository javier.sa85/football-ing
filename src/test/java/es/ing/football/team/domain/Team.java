package es.ing.football.team.domain;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Team {
    private String name;
    private String city;
    private String owner;
    private int stadiumCapacity;
    private String league;
    private int numPlayers;
    private LocalDate creationDate;
}