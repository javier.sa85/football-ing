CREATE TABLE TEAM (
    id                  IDENTITY NOT NULL PRIMARY KEY,
    name                VARCHAR(50) NOT NULL UNIQUE,
    city                VARCHAR(50) NOT NULL,
    owner               VARCHAR(50),
    stadium_capacity    INTEGER,
    league              VARCHAR(50) NOT NULL,
    num_players         INTEGER,
    creation_date       DATE
);

CREATE INDEX idx_team_name
    ON TEAM (name);

