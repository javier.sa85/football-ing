package es.ing.football.team.response;

import java.time.LocalDate;

import es.ing.football.team.entity.Team;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamResponse {
    private Long id;
    private String name;
    private String city;
    private String owner;
    private int stadiumCapacity;
    private String league;
    private int numPlayers;
    private LocalDate creationDate;

    /**
     * Converts the Team Entity into the TeamResponse used for REST interaction
     *
     * @param teamEntity
     * @return a TeamResponse based on the received Team
     */
    public static TeamResponse of(Team teamEntity) {
        TeamResponse team = new TeamResponse();
        team.setId(teamEntity.getId());
        team.setName(teamEntity.getName());
        team.setCity(teamEntity.getCity());
        team.setOwner(teamEntity.getOwner());
        team.setStadiumCapacity(teamEntity.getStadiumCapacity());
        team.setLeague(teamEntity.getLeague());
        team.setNumPlayers(teamEntity.getNumPlayers());
        team.setCreationDate(teamEntity.getCreationDate());

        return team;
    }

}
