package es.ing.football.team.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import es.ing.football.team.entity.Team;

public interface TeamRepository extends PagingAndSortingRepository<Team, Long>{
    Iterable<Team> findAll(Sort sort);
    Team findByName(String name);
}
