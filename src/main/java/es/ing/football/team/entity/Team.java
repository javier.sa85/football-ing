package es.ing.football.team.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {
    @Id
    private long id;
    
    @NotEmpty(message = "The name is required.")
    private String name;

    @NotEmpty(message = "The city is required.")
    private String city;
    private String owner;
    @Column(name="stadium_capacity")
    private int stadiumCapacity;

    @NotEmpty(message = "The league is required.")
    private String league;
    @Column(name="num_players")
    private int numPlayers;
    @Column(name="creation_date")
    private LocalDate creationDate;
    
}
