package es.ing.football.team.exceptions;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    private ResponseEntity<String> dataIntegrityViolation(ConstraintViolationException e){
        log.warn("Data Integrity Violation Exception Raised; Reason: " + e.getLocalizedMessage());
        StringBuilder errorMessage = new StringBuilder();
        e.getConstraintViolations().stream().map(t -> t.getMessage() + "\n").forEach(errorMessage::append);
        return new ResponseEntity<>(errorMessage.toString(), HttpStatus.BAD_REQUEST);
    }
}