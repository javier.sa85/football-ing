package es.ing.football.team.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.ing.football.team.response.TeamResponse;
import es.ing.football.team.service.TeamService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/teams")
@Slf4j
public class TeamController {
    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService){
        this.teamService = teamService;
    }

    @GetMapping("")
    public ResponseEntity<List<TeamResponse>> getAllTeams(
        @RequestParam(defaultValue = "id") String sortBy) {

        List<TeamResponse> teams = teamService.findAllTeams(Sort.by(sortBy));

        if (teams.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(teams, HttpStatus.OK);
        }
    }

    @GetMapping("{name}")
    public ResponseEntity<TeamResponse> findTeamByName(@PathVariable("name") String name){
        log.trace("Controller findTeamByName: "+ name);
        TeamResponse teamResponse = teamService.findTeamByName(name);

        return new ResponseEntity<>(teamResponse, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<TeamResponse> createTeam(@RequestBody TeamResponse team) {
        log.trace("Controller createTeam: "+ team);
        return new ResponseEntity<>(teamService.save(team), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<TeamResponse> updateTeam(@PathVariable("id") Long id, @RequestBody TeamResponse teamResponse) {
        return new ResponseEntity<>(teamService.updateTeam(id, teamResponse), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteTeam(@PathVariable("id") Long teamId) {
        teamService.deleteTeamById(teamId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}