package es.ing.football.team.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import es.ing.football.team.entity.Team;
import es.ing.football.team.exceptions.TeamNotFoundException;
import es.ing.football.team.repository.TeamRepository;
import es.ing.football.team.response.TeamResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TeamService {
    
    private final TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    /**
     * 
     * @param name  Team Name
     * @return a TeamResponse based on the information on the DDBB
     * @exception TeamNotFoundException when not found
     */
    public TeamResponse findTeamByName(String name){
        log.trace("Getting Team by name: " + name);
        Team team = teamRepository.findByName(name);
        if (null == team){
            throw new TeamNotFoundException();
        }

        return TeamResponse.of(team);
    }

    /**
     *
     * @return a List of TeamResponse with the information of all teams in the DDBB
     */
    public List<TeamResponse> findAllTeams(Sort sort) {
        log.trace("Getting all teams");
        Iterable<Team> teams = teamRepository.findAll(sort);

        return StreamSupport.stream(teams.spliterator(), true).map(t -> TeamResponse.of(t)).collect(Collectors.toList());
    }

    /**
     * Search the team by id in the DDBB and updates it
     * 
     * @param teamResponse teamResponse
     * @return The updated Team
     * @exception TeamNotFoundException if the ID don't exists in the database
     */
    public TeamResponse updateTeam(Long teamId, TeamResponse teamResponse) {
        log.trace("Updating team" + teamResponse);

        Team team = teamRepository.findById(teamId).orElseThrow(TeamNotFoundException::new);
        log.trace("Team found by Id" + teamId);

        team.setName(teamResponse.getName());
        team.setCity(teamResponse.getCity());
        team.setOwner(teamResponse.getOwner());
        team.setLeague(teamResponse.getLeague());
        team.setStadiumCapacity(teamResponse.getStadiumCapacity());
        team.setNumPlayers(teamResponse.getNumPlayers());
        team.setCreationDate(teamResponse.getCreationDate());
        return TeamResponse.of(teamRepository.save(team));
    }

    /**
     *
     * @param teamResponse Team to be saved
     * @return the saved team
     * @exception ContraintViolationException may be raised if the team is not valid
     */
    public TeamResponse save(TeamResponse teamResponse) {
        log.trace("Saving team" + teamResponse);
        Team team = new Team();
        if (null != teamResponse.getId()){
            team.setId(teamResponse.getId());
        }

        team.setName(teamResponse.getName());
        team.setCity(teamResponse.getCity());
        team.setOwner(teamResponse.getOwner());
        team.setStadiumCapacity(teamResponse.getStadiumCapacity());
        team.setLeague(teamResponse.getLeague());
        team.setNumPlayers(teamResponse.getNumPlayers());
        team.setCreationDate(teamResponse.getCreationDate());
        Team savedTeam = teamRepository.save(team);

        return TeamResponse.of(savedTeam);
    }

    public void deleteTeamById(Long teamId) {
        log.trace("Deleting team: " + teamId);
        teamRepository.deleteById(teamId);
    }
}
